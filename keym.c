#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <time.h>
#include <sys/select.h>
#include <X11/extensions/XTest.h>
#include <X11/keysym.h>

static const int idle_cutoff = 500; /* time before program exits by itself if no usage detected */
static const int speeds[5] = {80, 400, 1400, 4000, 10000}; /* mouse movement speeds */
static const int scroll[5] = {1000, 5000, 30000, 50000, 100000}; /* scrolling speeds */
static const char* unmap[] = {"w", "a", "s", "d", "q", "e", "r", "f", "g", "h", "j", "k", "l", "semicolon", "i", "c", "u", "o", "Shift_L", "backslash", "Tab", "Left", "Right", "Up", "Down", "x", "m", "Control_R", "F12"};

static Display *display;
static char keymap[32] = {0};

char pressed(int keycode)
{
    KeyCode c = XKeysymToKeycode(display, keycode);
    return (keymap[c / 8] & (1 << c % 8)) > 0;
}

_Bool timediffms (struct timespec * s1, struct timespec * s2, int limit) {
	return (s1->tv_sec - s2->tv_sec) * 1000 + (s1->tv_nsec - s2->tv_nsec) / 1e6 > limit;
}

int main()
{
    KeySym *keysyms, *original;
    fd_set in_fds;
    struct timeval tv;
    int x11_fd;
    int num_ready_fds;
    char idle = 1;
    char key_delta[6] = {0}; /* left, right, up, down, scroll up, scroll down */
    char speed = 2;          /* dash, fast, normal, slow, crawl */
    char quit = 0;
    int first_keycode, max_keycode, ks_per_keystroke;
    int num_keycodes;
    int i,j;
    int len = sizeof(unmap)/sizeof(unmap[0]);

    if (!(display = XOpenDisplay(NULL)))
    {
        fprintf(stderr, "Cannot open display.\n");
        return 1;
    }

    x11_fd = ConnectionNumber(display);

    /* get the full keymap and keep a backup of the original to restore when we're done */
    XDisplayKeycodes(display, &first_keycode, &max_keycode);
    num_keycodes = max_keycode - first_keycode + 1;

    original = XGetKeyboardMapping(display, first_keycode, num_keycodes, &ks_per_keystroke);
    keysyms  = XGetKeyboardMapping(display, first_keycode, num_keycodes, &ks_per_keystroke);

    /* unmap a selection of keys */
    for (i=0; i<num_keycodes; ++i)
    {
        if (keysyms[i*ks_per_keystroke] != NoSymbol)
        {
            KeySym ks = keysyms[i*ks_per_keystroke];
            const char* keysym_str = XKeysymToString(ks);

            /* uncomment if you want a list of strings for unmapping
            printf("%s\n", keysym_str);
            fflush(stdout);
             */

            for (j=0; j<len; ++j)
                if (strcmp(keysym_str, unmap[j]) == 0)
                    keysyms[i*ks_per_keystroke] = NoSymbol;
        }
    }

    /* do the unmapping */
    XChangeKeyboardMapping(display, first_keycode, ks_per_keystroke, keysyms, max_keycode-first_keycode);

    while (1)
    {
        FD_ZERO(&in_fds);
        FD_SET(x11_fd, &in_fds);

        tv.tv_sec = 0;
        tv.tv_usec = (key_delta[4] || key_delta[5]) ? scroll[speed] : idle ? 50000 : speeds[speed];
        num_ready_fds = select(x11_fd + 1, &in_fds, NULL, NULL, &tv);

        XQueryKeymap(display, keymap);

        /* mouse movement */
        key_delta[0] = pressed(XK_Left)  || pressed(XK_a);
        key_delta[1] = pressed(XK_Right) || pressed(XK_d);
        key_delta[2] = pressed(XK_Up)    || pressed(XK_w);
        key_delta[3] = pressed(XK_Down)  || pressed(XK_s);

        /* scrolling */
        key_delta[4] = pressed(XK_r);
        key_delta[5] = pressed(XK_f);

        /* speed adjustment from slow to fast */
        speed = 2;
        speed = (pressed(XK_g)) ? 4 : speed;
        speed = (pressed(XK_h) || pressed(XK_backslash) || pressed(XK_Tab)) ? 3 : speed;
        speed = (pressed(XK_l) || pressed(XK_Shift_L)) ? 1 : speed;
        speed = (pressed(XK_semicolon)) ? 0 : speed;

        /* mouse clicks */
        XTestFakeButtonEvent(display, Button1, (pressed(XK_j) || pressed(XK_q)) ? True : False, CurrentTime);
        XTestFakeButtonEvent(display, Button3, (pressed(XK_k) || pressed(XK_e)) ? True : False, CurrentTime);
        XTestFakeButtonEvent(display, Button2, (pressed(XK_i) || pressed(XK_c)) ? True : False, CurrentTime);
        XTestFakeButtonEvent(display, 8, pressed(XK_u) ? True : False, CurrentTime);
        XTestFakeButtonEvent(display, 9, pressed(XK_o) ? True : False, CurrentTime);

        /* exit */
        if (!pressed(XK_x) && !pressed(XK_m))
            quit = 1;

        if (
			quit == 1
			&& (
				pressed(XK_x)
				|| pressed(XK_m)
				|| pressed(XK_F12)
			)
		) {
            /* restore the original mapping */
            XChangeKeyboardMapping(display, first_keycode, ks_per_keystroke, original, max_keycode-first_keycode);
            XCloseDisplay(display);
            return 0;
        }

		if (quit == 1 && pressed (XK_Super_L) && pressed (XK_space))
		{
			const char * homedir = getenv ("HOME");
			const char * tmpfile = ".local/tmp/kbd";

			int homedir_len = strlen (homedir);
			int tmpfile_len = strlen (tmpfile);

			int path_len = homedir_len + tmpfile_len + 1;

			char * path = malloc (path_len + 1);

			snprintf (path, path_len + 1, "%s%s%s", homedir, "/", tmpfile);

			_Bool timepassed = 0;

			if (0 == access (path, F_OK)) {
				struct stat s;
				if (0 == stat (path, & s)) {
					struct timespec currtime;
					clock_gettime (CLOCK_REALTIME, & currtime);

					timepassed = timediffms (& currtime, & s.st_mtim, 200);
				}
			}

			free (path);

			if (1 == timepassed) {
				/* restore the original mapping */
				XChangeKeyboardMapping(display, first_keycode, ks_per_keystroke, original, max_keycode-first_keycode);
				XCloseDisplay(display);
				return 0;
			}
		}

        /* option to grab whole keyboard focus - this is useful for some applications that try to do their own input handling */
        if (0 == pressed(XK_Control_R))
            XGrabKeyboard(display, XDefaultRootWindow(display), False, GrabModeAsync, GrabModeAsync, CurrentTime);

        idle = 1;
        if (key_delta[0] || key_delta[1] || key_delta[2] || key_delta[3])
        {
            XWarpPointer(display, None, None, 0, 0, 0, 0, (key_delta[1] - key_delta[0]), (key_delta[3] - key_delta[2]));
            XSync(display, False);
            idle = 0;
        }
        if (key_delta[4])
        {
            XTestFakeButtonEvent(display, Button4, True, 1);
            XTestFakeButtonEvent(display, Button4, False, 1);
        }
        else if (key_delta[5])
        {
            XTestFakeButtonEvent(display, Button5, True, 1);
            XTestFakeButtonEvent(display, Button5, False, 1);
        }
    }

    XCloseDisplay(display);
    return 1;
}
